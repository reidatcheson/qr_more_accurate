#include <iostream>
#include <vector>
#include <algorithm>
#include <ranges>
#include <cstddef>
#include <random>
#include <fstream>
#include <cmath>



/**
 *
 * BLAS routines needed:
 *
 * xGEMV - matrix-vector product
 *
 * LAPACK routines needed:
 *
 * xGESV - solve system of linear equations using LU factorization and partial pivoting
 * xGETRF - LU factorization with partial pivoting
 * xGECON - estimate condition number of LU factorized matrix
 * xTRTRS - solve triangular system
 * xGEQRF - QR factorize matrix with householder method
 * xORMQR - apply Q represented as product of householder reflectors
 * 
 *
 */


/* Compute matrix-vector product: Y<---alpha*A*x + beta*y.*/
extern "C" void dgemv_(char* trans,int* m,int* n,double* alpha,double* A,int* lda,double* X,int* incx,double* beta,double* Y,int* incy);

/* Estimate condition number.*/
extern "C" void dgecon_(char* norm,int* n,double* A,int* lda,double* anorm,double* rcond,double* work,int* iwork,int* info);

/* Solve linear system by LU factorization. */
extern "C" void dgesv_(int* n,int* nrhs,double* A,int* lda,int* ipiv,double* B,int* ldb,int* info);

/* Solve triangular system. */
extern "C" void dtrtrs_(char* uplo,char* trans,char* diag,int* n,int* nrhs,double* A,int* lda,double* B,int* ldb,int* info);

/* QR factorize matrix with householder method. */
extern "C" void dgeqrf_(int* m,int* n,double* A,int* lda,double* tau,double* work,int* lwork,int* info);

/*Apply Q (stored in A) to matrix C.*/
extern "C" void dormqr_(char* side,char* trans,int* m,int* n,int* k,double* A,int* lda,double* tau,double* C,int* ldc,double* work,int* lwork,int* info);


/*The matrix for studies.*/
void make_matrix(int nx,int ny,double* A,double* coeffs);
/*Manufacture solution for study.*/
void make_solution(int m,double* x);

/* Simple matrix-vector product.*/
void matmul(int m,double* A,double* in,double* out);


/**
 * Helper routines.
 **/

/*
 * QR Factorize A, overwriting A. Put reflector weights in tau.
 */
void qr(int m,double* A,double* tau);

/*
 * Solve linear system for QR factorized A.
 */
void qrsolve(int m,double* A,double* tau,double* inout);

/*
 * Solve linear system for A (original A)
 */
void lusolve(int m,double* A,double* inout);

/*
 * Calculate inverse condition number of matrix A that has been LU factorized.
 */
double rcond(int m,double* A);

/*Compute matrix-vector product out=A*in.*/
void matmul(int m,double* A,double* in,double* out);


int main(int argc,char** argv){
  int nx=32;
  int ny=32;
  int m=nx*ny;
  int nruns=10000;
  int seed=12149;
  double cmin=-4.0;
  double cmax=4.0;
  double diag=0.0;

  std::vector<double> qr_errs(nruns);
  std::vector<double> lu_errs(nruns);
  std::vector<double> rconds(nruns);
  /*First run for QR factorization.*/
  {
    std::mt19937_64 g(seed);
    std::uniform_real_distribution<double> d(cmin,cmax);
    for(int i=0;i<nruns;i++){
      std::cout<<"QR run i="<<i<<std::endl;
      std::vector<double> A(m*m,0.0);
      std::vector<double> coeffs(6);
      for(int j=0;j<6;j++) coeffs[j]=d(g);
      /*increase likelihood matrix is invertible
       * by increasing size of diagonal.
       **/
      coeffs[0]+=diag;
      make_matrix(nx,ny,A.data(),coeffs.data());
      std::vector<double> x(m,0.0);
      std::vector<double> b(m,0.0);
      make_solution(m,x.data());
      matmul(m,A.data(),x.data(),b.data());


      /*Compute QR factorization of A.*/
      std::vector<double> tau(m,0.0);
      qr(m,A.data(),tau.data());

      /*Solve Ax=b, overwriting b*/
      qrsolve(m,A.data(),tau.data(),b.data());

      /*now b should approximately equal x.*/
      std::vector<double> relerrs(m,0.0);
      for(int j=0;j<m;j++){
        relerrs[j]=std::abs(x[j]-b[j])/std::abs(x[j]);
      }
      qr_errs[i]=*std::ranges::max_element(relerrs);
    }
  }

  /*Second run for LU factorization.*/
  {
    std::mt19937_64 g(seed);
    std::uniform_real_distribution<double> d(cmin,cmax);
    for(int i=0;i<nruns;i++){
      std::cout<<"LU run i="<<i<<std::endl;
      std::vector<double> A(m*m,0.0);
      std::vector<double> coeffs(6);
      for(int j=0;j<6;j++) coeffs[j]=d(g);
      /*increase likelihood matrix is invertible
       * by increasing size of diagonal.
       **/
      coeffs[0]+=diag;
      make_matrix(nx,ny,A.data(),coeffs.data());
      std::vector<double> x(m,0.0);
      std::vector<double> b(m,0.0);
      make_solution(m,x.data());
      matmul(m,A.data(),x.data(),b.data());


      /*Compute Solve Ax=b using LU factorization with partial pivoting.*/
      lusolve(m,A.data(),b.data());
      /*Compute approximate condition number of A.*/
      double inv_cond=rcond(m,A.data());
      rconds[i]=inv_cond;


      /*now b should approximately equal x.*/
      std::vector<double> relerrs(m,0.0);
      for(int j=0;j<m;j++){
        relerrs[j]=std::abs(x[j]-b[j])/std::abs(x[j]);
      }
      lu_errs[i]=*std::ranges::max_element(relerrs);
    }
  }

  std::ofstream qrf("qr.dat",std::ofstream::binary);
  qrf.write((char*)qr_errs.data(),qr_errs.size()*sizeof(double));


  std::ofstream luf("lu.dat",std::ofstream::binary);
  luf.write((char*)lu_errs.data(),lu_errs.size()*sizeof(double));

  std::ofstream rcs("rconds.dat",std::ofstream::binary);
  rcs.write((char*)rconds.data(),rconds.size()*sizeof(double));


}


double rcond(int m,double* A){
  double rcond_=0.0;
  double Anorm=12.0;
  char norm='1';
  int n=m;
  int lda=m;
  std::vector<double> work(4*m,0.0);
  std::vector<int> iwork(m,0);
  int info=0;
  dgecon_(&norm,&n,A,&lda,&Anorm,&rcond_,work.data(),iwork.data(),&info);
  return rcond_;
}

void qr(int m,double* A,double* tau){
  int n=m;
  int lda=m;
  int nb=32;
  int lwork=n*nb;
  std::vector<double> work(lwork,0.0);
  int info=0;

  dgeqrf_(&m,&n,A,&lda,tau,work.data(),&lwork,&info);
}

void make_matrix(int nx,int ny,double* A,double* coeffs){
  int m=nx*ny;
  std::fill(A,A+m*m,0.0);
  auto id = [&](int ix,int iy)->int{return ix + nx*iy;};
  for(int ix=0;ix<nx;ix++){
    for(int iy=0;iy<ny;iy++){
      double* B=A+id(ix,iy)*m;
      B[id(ix,iy)]=coeffs[0];
      if(ix>0){
        B[id(ix-1,iy)]=coeffs[1];
      }
      if(ix<nx-1){
        B[id(ix+1,iy)]=coeffs[2];
      }
      if(iy>0){
        B[id(ix,iy-1)]=coeffs[3];
      }
      if(iy<ny-1){
        B[id(ix,iy+1)]=coeffs[4];
      }
    }
  }
}


void matmul(int m,double* A,double* in,double* out){
  char trans='N';
  int n=m;
  double alpha=1.0;
  int lda=m;
  double* X=in;
  double beta=0.0;
  int incx=1;
  double* Y=out;
  int incy=1;
  dgemv_(&trans,&m,&n,&alpha,A,&lda,X,&incx,&beta,Y,&incy);
}

void make_solution(int m,double* x){
  std::mt19937_64 g(2397423);
  std::uniform_real_distribution<double> d(-1.0,1.0);
  for(int i=0;i<m;i++){
    x[i]=d(g);
  }
}

void qrsolve(int m,double* A,double* tau,double* inout){

  /*First compute c=Q^T * b .*/
  {
    char side='L';
    char trans='T';
    int n=1;
    int k=m;
    int lda=m;
    double* C=inout;
    int ldc=m;
    int nb=32;
    int lwork=nb*n;
    std::vector<double> work(lwork,0.0);
    int info=0;
    dormqr_(&side,&trans,&m,&n,&k,A,&lda,tau,C,&ldc,work.data(),&lwork,&info);
  }
  /*Now do back substitution for Rx=c.*/
  {
    char uplo='U';
    char trans='N';
    char diag='N';
    int n=m;
    int nrhs=1;
    int lda=m;
    double* B=inout;
    int ldb=n;
    int info=0;
    dtrtrs_(&uplo,&trans,&diag,&n,&nrhs,A,&lda,B,&ldb,&info);
  }
}

void lusolve(int m,double* A,double* inout){
  int n=m;
  int nrhs=1;
  int lda=m;
  std::vector<int> ipiv(m,0);
  double* B=inout;
  int ldb=m;
  int info=0;
  dgesv_(&n,&nrhs,A,&lda,ipiv.data(),B,&ldb,&info);
}

