import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt



def make_matrix(nx,ny,coeffs):
    m=nx*ny
    A=np.zeros((m,m))
    def id(ix,iy):
        return ix + nx*iy

    for ix in range(0,nx):
        for iy in range(0,ny):
            A[id(ix,iy),id(ix,iy)]=coeffs[0]
            if ix>0:
                A[id(ix-1,iy),id(ix,iy)]=coeffs[1]
            if ix<nx-1:
                A[id(ix+1,iy),id(ix,iy)]=coeffs[2]
            if iy>0:
                A[id(ix,iy-1),id(ix,iy)]=coeffs[3]
            if iy<ny-1:
                A[id(ix,iy+1),id(ix,iy)]=coeffs[4]
    return A



nx=32
ny=32
nbins=100
nsamples=10000

symms=np.zeros(nsamples)
norms=np.zeros(nsamples)
conds=np.zeros(nsamples)
for i in range(0,nsamples):
    print(i)
    coeffs=np.random.uniform(-4.0,4.0,5)
    A=make_matrix(nx,ny,coeffs)
    D=np.linalg.norm(A.T - A,ord=1)/np.linalg.norm(A,ord=1)
    N=np.linalg.norm( A @ A.T - A.T @ A, ord=1) / 0.5*np.linalg.norm(A @ A.T + A.T @ A, ord=1)
    symms[i]=D
    norms[i]=N
    conds[i]=np.linalg.cond(A,p=1)



plt.hist(symms,bins=nbins)
plt.title("Loss of symmetry")
plt.xlabel("$\\frac{||A ^ T - A ||}{|| A + A^T ||}$ ")
plt.ylabel("Frequency")
plt.savefig("symm.svg")
plt.close()

plt.hist(norms,bins=nbins)
plt.title("Loss of normality")
plt.ylabel("Frequency")
plt.xlabel("$\\frac{||A ^ T A  - A A ^T  ||}{|| AA^T + A^T A ||}$")
plt.savefig("norms.svg")
plt.close()
plt.hist(np.log(conds),bins=nbins)
plt.savefig("conds.svg")
plt.title("Logarithm of condition numbers")
plt.ylabel("Frequency")
plt.xlabel("$\log  (\kappa (A)) $")
plt.savefig("conds.svg")

