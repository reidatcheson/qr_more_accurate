

CXXFLAGS += -O3 --std=c++20


main : main.cpp
	$(CXX) $(CXXFLAGS) main.cpp -o main -llapack -lblas


.PHONY : clean


clean :
	rm -rf ./main
	rm -rf ./*.svg
	rm -rf ./*.dat
