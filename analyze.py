import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt



lu=np.fromfile("lu.dat")
qr=np.fromfile("qr.dat")
rconds=np.fromfile("rconds.dat")


nbins=100

plt.hist(np.log(lu),bins=nbins);
plt.hist(np.log(qr),bins=nbins);
plt.title("Distribution of relative errors for LU and QR solves")
plt.legend(["LU relative errors","QR relative errors"]);
plt.xlabel("Logarithm of relative errors")
plt.ylabel("Frequency")
plt.savefig("errs.svg")
plt.close()




res=qr/lu
tmp=np.array(list(filter(lambda x : x<20,res)))
plt.hist(tmp,bins=nbins);
plt.title("Distribution of ratio of relative errors for LU and QR solves")
plt.xlabel("Ratio of relative errors")
plt.ylabel("Frequency")
plt.savefig("ratio_errs.svg")
plt.close()


#plt.close()
#plt.hist(np.log(rconds),bins=nbins)
#plt.title("Condition Numbers")
#plt.xlabel("logarithm of inverse condition number")
#plt.ylabel("frequency")
#plt.savefig("rconds.svg")
#plt.close()
#
#
#plt.scatter(np.log(rconds),np.log(lu))
#plt.scatter(np.log(rconds),np.log(qr))
#plt.title("Error as function of conditioning")
#plt.xlabel("Inverse condition number")
#plt.ylabel("Relative errors")
#plt.legend(["LU errors","QR errors"])
#plt.savefig("errcond.svg")
#plt.close()
#
#plt.scatter(np.log(lu),np.log(qr))
#plt.title("QR errors as function of LU errors")
#plt.xlabel("lu errors")
#plt.ylabel("qr errors")
#plt.savefig("qrlu.svg")
#plt.close()


print(np.mean(np.log(rconds)))
print()
print(np.mean(np.log(lu)))
print(np.std(np.log(lu)))
print(np.median(np.log(lu)))
print()

print(np.mean(np.log(qr)))
print(np.std(np.log(qr)))
print(np.median(np.log(qr)))
